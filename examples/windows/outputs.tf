//outputs from module vsphere_vm_windows
output "vm_names" {
value = module.vsphere_vm_windows.name
}
output "vm_addresses" {
value = module.vsphere_vm_windows.default_ip_address
}