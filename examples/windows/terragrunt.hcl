inputs = {
  vm_template_name = "Win64_2012_template_001"
  vm_name_prefix = "Nobby"
  vm_num_cpus = 2
  vm_memory = 2048
  vm_disk_size_gb = 60
  vm_disk_name = "disk0"
  vm_subnet_first_address = "192.168.1.60"
  vm_subnetmask = 24
}

include {
  path = find_in_parent_folders()
}