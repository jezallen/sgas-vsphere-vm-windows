data "vsphere_datacenter" "dc" {
  name = var.vcenter_dc_name
}
data "vsphere_compute_cluster" "compute_cluster" {
  name            = var.vcenter_compute_cluster
  datacenter_id   = data.vsphere_datacenter.dc.id
}
data "vsphere_datastore_cluster" "datastore_cluster" {
  name          = var.vcenter_datastore_cluster
  datacenter_id = data.vsphere_datacenter.dc.id
}
data "vsphere_network" "network" {
  name          = var.vcenter_network_name
  datacenter_id = data.vsphere_datacenter.dc.id
}
data "vsphere_virtual_machine" "template" {
  name          = var.vm_template_name
  datacenter_id = data.vsphere_datacenter.dc.id
}
