locals {
  ipv4_octets = split(".", var.vm_subnet_first_address)
}

resource "vsphere_virtual_machine" "vsphere_vm_windows" {
  count            = var.vm_count
  name             = "${var.vm_name_prefix}${format("%03d", count.index + 1)}"
  resource_pool_id     = data.vsphere_compute_cluster.compute_cluster.resource_pool_id
  datastore_cluster_id = data.vsphere_datastore_cluster.datastore_cluster.id

  num_cpus = var.vm_num_cpus
  memory   = var.vm_memory
  guest_id = data.vsphere_virtual_machine.template.guest_id
  scsi_type = data.vsphere_virtual_machine.template.scsi_type
  enable_logging = true
  //wait_for_guest_net_routable = false
  //wait_for_guest_net_timeout = 0
  //wait_for_guest_net_timeout = -1
  //ignored_guest_ips = ["192.168.1.207","192.168.1.208","192.168.1.209","192.168.1.210","192.168.1.211","192.168.1.212"]
  ignored_guest_ips = ["192.168.1.207/26"]

  network_interface {
    network_id = data.vsphere_network.network.id
  }

  disk {
    label = var.vm_disk_name
    size  = var.vm_disk_size_gb
    thin_provisioned = data.vsphere_virtual_machine.template.disks.0.thin_provisioned
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id
    timeout = 360

    customize {
      windows_options {
        computer_name = "${var.vm_name_prefix}${format("%03d", count.index + 1)}"
        workgroup    = var.global_domain
      }

      network_interface {
        // increment the final octet of vm_subnet_first_address
        ipv4_address = "${join(".", slice(local.ipv4_octets, 0, 3))}.${element(local.ipv4_octets, 3) + count.index}"
        ipv4_netmask = var.vm_subnetmask
      }

      ipv4_gateway = var.global_ipv4_gateway
      dns_server_list = var.global_dns_servers
    }
  }
}