output "default_ip_address" {
 value = vsphere_virtual_machine.vsphere_vm_windows.*.default_ip_address
}
output "name" {
 value = vsphere_virtual_machine.vsphere_vm_windows.*.name
}